package app;

import java.math.BigInteger;
import java.util.Arrays;

public class Main {

    private int threshold = 1000;

    public static void main(final String[] args) {

        final Main app = new Main();

        Amount amount = new Amount(new BigInteger("999"));
        if (!app.approval(amount)) {
            System.out.println(amount + " does not require approval");
        }
        amount = new Amount(new BigInteger("2000"));
        if (app.approval(amount)) {
            System.out.println(amount + " requires approval");
        }
    }

    public boolean approval(final Amount amount) {
       if(amount.getValue() >= threshold) {
           return true;
       }
       return false;
   }

}

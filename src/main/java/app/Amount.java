package app;

import java.math.BigInteger;

public final class Amount {

    private final Integer value;

    Amount(final BigInteger value) {
        if (value.compareTo(new BigInteger("0")) < 0) {
            throw new RuntimeException("Negative number");
        }
        if (value.compareTo(new BigInteger("10000000")) > 0) {
            throw new RuntimeException("Too big");
        }

        this.value = value.intValue();
    }

    /**
     * @return the value
     */
    public Integer getValue() {
        return value;
    }
}
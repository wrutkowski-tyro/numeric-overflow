package app;

import java.math.BigInteger;
import org.junit.jupiter.api.DisplayName;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.Tag;
import java.util.List;
import static org.junit.jupiter.api.Assertions.assertTrue;

@DisplayName("Security unit tests")
@Tag("security")
public class appSecuritySpec {

    @Test
    public void BiggerThanIntMaxSizeNeedsApproval() {
        Main app = new Main();
        Boolean testThrows = false;
        try {
            boolean res = app.approval(new Amount(new BigInteger("2147483649")));
        } catch (Exception e) {
            testThrows = true;
        }
        assertTrue(testThrows, () -> "Bigger than int max size throws");
    }

    @Test
    public void LessThanIntMinSizeNeedsApproval() {
        Main app = new Main();
        Boolean testThrows = false;
        try {
            boolean res = app.approval(new Amount(new BigInteger("-2147483649")));
        } catch (Exception e) {
            testThrows = true;
        }
        assertTrue(testThrows, () -> "Less than int min size throws");
    }

    @Test 
    public void AmountCannotAcceptNegativeValue() {
        Boolean testThrows = false;
        try {
            Amount amount = new Amount(new BigInteger("-1"));
        } catch (Exception e) {
            testThrows = true;
        }
        assertTrue(testThrows, () -> "Amount should throw for negative value");
    }

    @Test 
    public void AmountCannotAcceptOver10MilValue() {
        Boolean testThrows = false;
        try {
            Amount amount = new Amount(new BigInteger("10000001"));
        } catch (Exception e) {
            testThrows = true;
        }
        assertTrue(testThrows, () -> "Amount should throw for too big value");
    }
}
